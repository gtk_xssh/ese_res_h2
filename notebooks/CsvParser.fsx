open System
open System.IO
open Inputs

let ParseCsvLine (line: string) =
   let fields = line.Split([|";"|], StringSplitOptions.TrimEntries) |> List.ofArray
   let date = DateTime.ParseExact(fields.[0], 
                @"dd/MM/yyyy HH:mm", 
                Globalization.CultureInfo.InvariantCulture)
   let pvOut = fields.[1].Replace(',', '.')
   let windOut = fields.[2].Replace(',', '.')
   {
       PvWindNominalPower.Day = date.Day
       Month = date.Month
       Hours = date.Hour
       PvOut1MWh = float pvOut
       WindOut1MWh = float windOut
   }

let ParseFile filename = 
    File.ReadLines filename
    |> Seq.skip 1
    |> Seq.filter (fun line -> line.Length > 10)
    |> Seq.map ParseCsvLine
    |> List.ofSeq
