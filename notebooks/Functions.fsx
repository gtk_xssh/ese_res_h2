#load "Inputs.fsx"
#load "Outputs.fsx"
#load "CsvParser.fsx"
#load "LinearRegression.fsx"

open Inputs
open Outputs
open LinearRegression


//Battery
let BatteryWithDegradation (battery: Battery) =
    battery.Degradation
    |> List.mapi (fun index degradation ->
        { BatteryOutput.YearOfOperation = battery.YearOfConstruction + index
          MWh =
            float battery.HoursCapacity
            * battery.PowerOutput
            * (1.0 + battery.ExtraCapacity / 100.0)
            * degradation
            / 100.0
          MW =
            battery.PowerOutput
            * (1.0 + battery.ExtraCapacity / 100.0)
            * degradation
            / 100.0 })

// PV and Wind Section
let PvAndWindWithDegradation (baseData: PvWindNominalPower list) (pv: PV) (wind: Wind) (year: int) =
    let idx = year - 1
    let selectedYear = pv.YearOfConstruction + idx
    let pvDegFactor = pv.Degradation.[idx] / 100.0
    let windDegFactor = wind.Degradation.[idx] / 100.0

    baseData
    |> List.map (fun data ->
        { PvWindOutput.Day = new System.DateTime(selectedYear, data.Month, data.Day, data.Hours, 0, 0)
          PvOut = data.PvOut1MWh * pvDegFactor * pv.Size
          WindOut = data.WindOut1MWh * windDegFactor * wind.Size })

// Questa funzione e quella successiva sono utili solo in fase di visualizzazione
let PvWithDegradation (baseData: PvWindNominalPower list) (pv: PV) =
    let rowData =
        baseData
        |> List.map (fun e ->
            (new System.DateTime(pv.YearOfConstruction, e.Month, e.Day, e.Hours, 0, 0),
             pv.Degradation
             |> List.map (fun d -> e.PvOut1MWh * pv.Size * d / 100.0)))

    let len = pv.Degradation.Length

    { EnergyDegradationOverYears.SerieName = "PV"
      YearsOfOperation = [ 1 .. len ]
      Years =
        [ 1 .. len ]
        |> List.map (fun i -> pv.YearOfConstruction - 1 + i)
      Rows = rowData }

let WindWithDegradation (baseData: PvWindNominalPower list) (wind: Wind) =
    let rowData =
        baseData
        |> List.map (fun e ->
            (new System.DateTime(wind.YearOfConstruction, e.Month, e.Day, e.Hours, 0, 0),
             wind.Degradation
             |> List.map (fun d -> e.WindOut1MWh * wind.Size * d / 100.0)))

    let len = wind.Degradation.Length

    { EnergyDegradationOverYears.SerieName = "Wind"
      YearsOfOperation = [ 1 .. len ]
      Years =
        [ 1 .. len ]
        |> List.map (fun i -> wind.YearOfConstruction - 1 + i)
      Rows = rowData }

// Biomass
let BiomassCalculator (biomassa: BiomassGasifier) =
    let tp =
        biomassa.StrawMotorSize
        / (biomassa.ElectricEfficiency / 100.0)

    let mbc1h =
        tp * 1000.0
        / (biomassa.BiomassLHV / 3600.0)
        / 1000.0

    { ThermalPower = tp
      MaxBiomassConsumptionOneHour = mbc1h
      BiomassConsumption = mbc1h / biomassa.StrawMotorSize
      MinimumLoad =
        biomassa.StrawMotorSize
        * biomassa.MinimumMotorLoad
        / 100.0 }


// Sezione Elettrolizzatori
let CalcElectrolizersTablesOutput (el_input: Electrolyzers) (el_output: ElectrolyzersOutput) (yearOfOp: int) =
    let year =
        el_input.YearOfConstruction + yearOfOp - 1

    let loads =
        el_output.EnergyConsumptionOutput
        |> List.map (fun ec -> ec.Load)

    let productionTemp =
        el_output.EnergyConsumptionOutput
        |> List.map (fun e ->
            el_input.PowerDcConsumption * 1000.0 * e.Load
            / 100.0
            / e.ConsumptionDC)
    //Ogni dieci anni i dati si resettano
    let counter =
        if yearOfOp % 10 = 0 then
            10
        else
            yearOfOp % 10

    let dcConsum1Line100perc =
        [ 2 .. counter ]
        |> List.map (fun e -> float e)
        |> List.fold
            (fun accumulator value -> accumulator * (1.0 + el_input.Degradation / 100.0))
            (el_input.PowerDcConsumption * 1000.0)

    let dcConsum1Lines =
        el_output.EnergyConsumptionOutput
        |> List.map (fun e -> e.Load / 100.0 * dcConsum1Line100perc)

    let consDiff =
        el_output.EnergyConsumptionOutput
        |> List.map (fun ec -> ec.ConsumptionTot - ec.ConsumptionDC)

    let totConsum =
        (consDiff, dcConsum1Lines, productionTemp)
        |||> List.map3 (fun ec dc prod -> ec * prod + dc)

    let specCons =
        (totConsum, productionTemp)
        ||> List.map2 (fun cons prod -> cons / prod)

    let intercept, slope = Fit totConsum specCons

    { ElectrolizersTablesOutput.Year = year
      YearOfOp = yearOfOp
      Loads = loads
      DcConsum1LineWithDegradationAtPartialLoad = dcConsum1Lines
      TotConsum1LineWithDegradationAtPartialLoad = totConsum
      SpecificConsumption = specCons
      MinimumLoadOfOneLine =
        el_output.MinimumLoadOfOneLine
        * (specCons |> List.last)
        / 1000.0
      Slope = slope
      Intercect = intercept }


let ElectrolyzersWithDegradationStep1 (electrolyzers: Electrolyzers) =
    let hsvn = 11.128223495702 // HydrogenSpecificVolumeNTP

    { //First Entry
      ElectrolyzersOutput.PowerDcConsumptionTot =
        electrolyzers.PowerDcConsumption
        * float electrolyzers.Lines
      NominalH2ProductionTot =
        electrolyzers.NominalH2Production
        * float electrolyzers.Lines
      WaterConsumption = electrolyzers.WaterConsumption / 1000.0
      WaterDischarged =
        electrolyzers.WaterConsumption / 1000.0
        * electrolyzers.WaterDischarge
        / 100.0
      OxygenProduction = 8.0
      HydrogenSpecificVolumeNTP = hsvn
      MinimumLoadOfOneLine =
        electrolyzers.NominalH2Production
        * electrolyzers.MinimumLoadOf1Line
        / 100.0
      OneLineAtDifferentLoads =
        [ { //First Entry
            Load = 100.0
            NominalH2Production = electrolyzers.NominalH2Production
            CompressorConsumption = electrolyzers.OneLineAtMaxLoad.CompressorConsumption
            OtherAux = electrolyzers.OneLineAtMaxLoad.OtherAux
            ElectricalLosses = electrolyzers.OneLineAtMaxLoad.ElectricalLosses
            WaterPumps = electrolyzers.OneLineAtMaxLoad.WaterPumps
            CompressorOxygen = electrolyzers.OneLineAtMaxLoad.CompressorOxygen } ]
      EnergyConsumptionOutput =
        electrolyzers.EnergyConsumption
        |> List.map (fun ec ->
            { EnergyConsumptionOutput.Load = ec.Load
              ConsumptionDC = hsvn * ec.Consumption * 0.976
              ConsumptionAC = hsvn * ec.Consumption
              ConsumptionTot = 0.0 })
      ConsumptionOverYears = [] }

let ElectrolyzersWithDegradationStep2 (el_input: Electrolyzers) (el_output: ElectrolyzersOutput) =
    let temp =
        List.skip 1 el_output.EnergyConsumptionOutput

    let items =
        temp
        |> List.map (fun ec ->
            let watProd =
                el_input.PowerDcConsumption * 10.0 * ec.Load
                / ec.ConsumptionDC

            { OneLineLoad.Load = ec.Load
              NominalH2Production = watProd
              CompressorConsumption =
                watProd / el_input.NominalH2Production
                * el_input.OneLineAtMaxLoad.CompressorConsumption
              OtherAux =
                watProd / el_input.NominalH2Production
                * el_input.OneLineAtMaxLoad.OtherAux
              ElectricalLosses =
                watProd / el_input.NominalH2Production
                * el_input.OneLineAtMaxLoad.ElectricalLosses
              WaterPumps =
                watProd / el_input.NominalH2Production
                * el_input.OneLineAtMaxLoad.WaterPumps
              CompressorOxygen =
                watProd / el_input.NominalH2Production
                * el_input.OneLineAtMaxLoad.CompressorOxygen })

    let finalItems =
        el_output.OneLineAtDifferentLoads @ items

    { el_output with OneLineAtDifferentLoads = finalItems }

let ElectrolyzersWithDegradationStep3 (el_input: Electrolyzers) (el_output: ElectrolyzersOutput) =
    let cooling =
        el_input.CoolingSystemConsumption
        + el_input.GasManagementConsumption

    let items =
        (el_output.EnergyConsumptionOutput, el_output.OneLineAtDifferentLoads)
        ||> List.map2 (fun ec oneLine ->
            let oneLineSum =
                oneLine.CompressorConsumption
                + oneLine.OtherAux
                + oneLine.ElectricalLosses
                + oneLine.WaterPumps
                + oneLine.CompressorOxygen

            { ec with
                ConsumptionTot =
                    ((ec.ConsumptionAC * oneLine.NominalH2Production)
                     + (oneLineSum * 1000.0)
                     + (cooling * oneLine.NominalH2Production))
                    / oneLine.NominalH2Production })

    { el_output with EnergyConsumptionOutput = items }

let ElectrolyzersWithDegradationStep4 (el_input: Electrolyzers) (el_output: ElectrolyzersOutput) =
    let tableOut =
        [ 1 .. 20 ]
        |> List.map (fun year -> CalcElectrolizersTablesOutput el_input el_output year)

    { el_output with ConsumptionOverYears = tableOut }


let ElectrolyzersWithDegradation (electrolyzers: Electrolyzers) =
    ElectrolyzersWithDegradationStep1 electrolyzers
    |> ElectrolyzersWithDegradationStep2 electrolyzers
    |> ElectrolyzersWithDegradationStep3 electrolyzers
    |> ElectrolyzersWithDegradationStep4 electrolyzers

// Sezione Calculation Year
let CalculateYearRow
    (prevValue: CalculationYearRow)
    (currentValue: CalculationYearRow)
    (el_output: ElectrolyzersOutput)
    (lines: int)
    (manteinanceMonth: int)
    (strawMotorSize: float)
    (biomass: BiomassGasifierOutput)
    (batteryEfficency: float)
    (batteryOutput: BatteryOutput list)
    (minimumH2Production: float)
    =
    let year = currentValue.Day.Year
    let month = currentValue.Day.Month

    let NpResNonProgrammable =
        currentValue.PvOut + currentValue.WindOut

    let MaxLoadCandidate =
        (el_output.ConsumptionOverYears
         |> List.find (fun e -> e.Year = year))
            .TotConsum1LineWithDegradationAtPartialLoad
            .Head
        * (float lines)

    let MaxLoad =
        if manteinanceMonth = month then
            0.0
        else
            MaxLoadCandidate

    let NpResMinusMaxLoad = NpResNonProgrammable - MaxLoad

    let PResProgrammable =
        match NpResMinusMaxLoad with
        | v when v > (biomass.MinimumLoad * -1000.0) -> 0.0
        | v when v > (strawMotorSize * -1000.0) -> v * -1.0
        | _ -> strawMotorSize * 1000.0

    let BiomassForEEProduction =
        PResProgrammable * biomass.BiomassConsumption

    let NpResAddPResMinusMaxLoad = NpResMinusMaxLoad + PResProgrammable

    let ToElectrolyzer =
        if NpResAddPResMinusMaxLoad < 0 then
            NpResNonProgrammable + PResProgrammable
        else
            MaxLoad

    let NeededFromStorage =
        if NpResAddPResMinusMaxLoad <= 0 then
            NpResAddPResMinusMaxLoad * -1.0
        else
            0.0

    let minOneLoadTemp =
        (el_output.ConsumptionOverYears
         |> List.find (fun e -> e.Year = year))
            .MinimumLoadOfOneLine
        * 1000.0

    let PotentiallyToStorage =
        if NpResAddPResMinusMaxLoad >= 0.0 then
            NpResAddPResMinusMaxLoad
        elif ToElectrolyzer < minOneLoadTemp then
            ToElectrolyzer
        else
            0.0

    let PotentiallyFromStorage =
        PotentiallyToStorage * batteryEfficency / 100.0

    let BatterySoC =
        let battMWh =
            (batteryOutput
             |> List.find (fun e -> e.YearOfOperation = year))
                .MWh
            * 1000.0

        let battMW =
            (batteryOutput
             |> List.find (fun e -> e.YearOfOperation = year))
                .MW
            * 1000.0

        let tempExp =
            if NeededFromStorage > 0
               && PotentiallyToStorage > 0
               && (prevValue.BatterySoC + ToElectrolyzer) < minOneLoadTemp then
                0.0
            else
                min NeededFromStorage battMW

        if ToElectrolyzer = 0.0
           && prevValue.BatterySoC < minOneLoadTemp then
            prevValue.BatterySoC
        else
            min
                battMWh
                (max
                    0.0
                    (prevValue.BatterySoC
                     + min PotentiallyFromStorage battMW
                     - tempExp))

    let ChargingDischarging = BatterySoC - prevValue.BatterySoC

    let ActualFromStorage =
        if ChargingDischarging <= 0 then
            ChargingDischarging * -1.0
        else
            0.0

    let EEToElectrolyser =
        let firstOp = ToElectrolyzer + ActualFromStorage

        let secondOp =
            if firstOp > minOneLoadTemp then
                0.0
            elif ToElectrolyzer < minOneLoadTemp then
                PotentiallyToStorage
            else
                0.0

        firstOp - secondOp

    let PotentialEEFromGrid = MaxLoad - EEToElectrolyser

    let LinesWorking =
        let elec100 =
            (el_output.ConsumptionOverYears
             |> List.find (fun e -> e.Year = year))
                .DcConsum1LineWithDegradationAtPartialLoad
                .Head

        match EEToElectrolyser with
        | v when v = 0 -> 0
        | v when v < elec100 -> 1
        | v when v >= elec100 && v <= (elec100 * 2.0) -> 2
        | _ -> 3

    let EEToEachLine =
        if EEToElectrolyser = 0 then
            0.0
        else
            EEToElectrolyser / float LinesWorking

    let AreElectrolyzersWorkingUnderMiniumLoad =
        if LinesWorking > 0 && EEToEachLine < minOneLoadTemp then
            1
        else
            0

    let SpecificConsumption =
        let c =
            el_output.ConsumptionOverYears
            |> List.find (fun e -> e.Year = year)

        EEToEachLine * c.Slope + c.Intercect

    let Module1 =
        if LinesWorking = 1 then
            EEToElectrolyser / SpecificConsumption
        else
            0.0

    let Module2 =
        if LinesWorking = 2 then
            EEToElectrolyser / SpecificConsumption
        else
            0.0

    let Module3 =
        if LinesWorking = 3 then
            EEToElectrolyser / SpecificConsumption
        else
            0.0

    let TotalH2Production = Module1 + Module2 + Module3
    let HourWithoutH2Production = if TotalH2Production > 0 then 0 else 1

    let WaterConsumption =
        TotalH2Production * el_output.WaterConsumption

    let WaterDischarge =
        TotalH2Production * el_output.WaterDischarged

    let EnergyToGrid =
        if ChargingDischarging >= 0 then
            PotentiallyToStorage
            - (ChargingDischarging / (batteryEfficency / 100.0))
        else
            0.0

    let O2Production =
        TotalH2Production * el_output.OxygenProduction

    let N2Consumption = 0.0

    let H2ToBeProduced =
        if (minimumH2Production - TotalH2Production) <= 0 then
            0.0
        else
            minimumH2Production - TotalH2Production

    let EnergyFromGrid = H2ToBeProduced * SpecificConsumption

    { CalculationYearRow.Day = currentValue.Day
      PvOut = currentValue.PvOut
      WindOut = currentValue.WindOut
      NpResNonProgrammable = NpResNonProgrammable
      MaxLoad = MaxLoad
      NpResMinusMaxLoad = NpResMinusMaxLoad
      PResProgrammable = PResProgrammable
      BiomassForEEProduction = BiomassForEEProduction
      NpResAddPResMinusMaxLoad = NpResAddPResMinusMaxLoad
      ToElectrolyzer = ToElectrolyzer
      NeededFromStorage = NeededFromStorage
      PotentiallyToStorage = PotentiallyToStorage
      PotentiallyFromStorage = PotentiallyFromStorage
      BatterySoC = BatterySoC
      ChargingDischarging = ChargingDischarging
      ActualFromStorage = ActualFromStorage
      EEToElectrolyser = EEToElectrolyser
      PotentialEEFromGrid = PotentialEEFromGrid
      LinesWorking = LinesWorking
      EEToEachLine = EEToEachLine
      AreElectrolyzersWorkingUnderMiniumLoad = AreElectrolyzersWorkingUnderMiniumLoad
      SpecificConsumption = SpecificConsumption
      Module1 = Module1
      Module2 = Module2
      Module3 = Module3
      TotalH2Production = TotalH2Production
      HourWithoutH2Production = HourWithoutH2Production
      WaterConsumption = WaterConsumption
      WaterDischarge = WaterDischarge
      EnergyToGrid = EnergyToGrid
      O2Production = O2Production
      N2Consumption = N2Consumption
      H2ToBeProduced = H2ToBeProduced
      EnergyFromGrid = EnergyFromGrid }

let CalculationYear (inputs: SystemInputs) (yearOfOp: int) =
    let initData =
        PvAndWindWithDegradation inputs.PvWindHourlyData inputs.PV inputs.Wind yearOfOp
        |> List.map (fun d ->
            { defaultCalculationYearRow with
                Day = d.Day
                PvOut = d.PvOut
                WindOut = d.WindOut })

    let el_output =
        ElectrolyzersWithDegradation inputs.Electrolyzers

    let lines = inputs.Electrolyzers.Lines
    let manteinanceMonth = inputs.ManteinanceMonth
    let strawMotorSize = inputs.BiomassGasifier.StrawMotorSize
    let biomass = BiomassCalculator inputs.BiomassGasifier
    let batteryEfficency = inputs.Battery.Efficiency
    let batteryOutput = BatteryWithDegradation inputs.Battery
    let minimumH2Production = inputs.Load.MinimumH2Production

    let scanFunc (prevValue: CalculationYearRow) (currentValue: CalculationYearRow) =
        CalculateYearRow
            prevValue
            currentValue
            el_output
            lines
            manteinanceMonth
            strawMotorSize
            biomass
            batteryEfficency
            batteryOutput
            minimumH2Production

    let allData =
        initData
        |> List.scan scanFunc defaultCalculationYearRow
        |> List.skip 1

    { CalculationYearOutput.Rows = allData
      YearOfOperation = yearOfOp
      Year = allData.Head.Day.Year }
