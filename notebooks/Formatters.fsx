#load "Functions.fsx"

open Inputs
open Outputs

let printAsInt (writer: System.IO.TextWriter) (desc: string) (x : float)  =
   writer.WriteLine(desc + " = {0:N0}</br>",x)

let printAs2dec (writer: System.IO.TextWriter) (desc: string) (x : float)  =
   writer.WriteLine(desc + " = {0:N2}</br>",x)
   
let writeHeader (writer: System.IO.TextWriter) (desc: string) =
   writer.Write("<th>{0}</th>", desc)

let writeintCol (writer: System.IO.TextWriter) (x : int)  =
   writer.WriteLine("<td>{0}</td>",x)

let write0decCol (writer: System.IO.TextWriter) (x : float)  =
   writer.WriteLine("<td>{0:N0}</td>",x)

let write2decCol (writer: System.IO.TextWriter) (x : float)  =
   writer.WriteLine("<td>{0:N2}</td>",x)

// Battery With Degradation
Formatter.SetPreferredMimeTypesFor(typeof<list<BatteryOutput>> ,"text/html")

let formatBatteryWithDegradation (b: list<BatteryOutput>) (writer: System.IO.TextWriter) =
   writer.WriteLine("<h3>Battery Data Output</h3>")
   writer.WriteLine("<table><thead><tr>")
   ["Year"; "Capacity MWh"; "Power MW"] |> List.iter (fun item -> writeHeader writer item)
   writer.WriteLine("</tr><thead><tbody>")
   for i in b do
      writer.WriteLine("<tr>")
      writeintCol writer i.YearOfOperation
      write0decCol writer i.MWh
      write0decCol writer i.MW
      writer.WriteLine("</tr>")
   writer.WriteLine("</tbody></table>")

Formatter.Register<list<BatteryOutput>>(formatBatteryWithDegradation, "text/html")

//Power Degradation
Formatter.SetPreferredMimeTypesFor(typeof<EnergyDegradationOverYears> ,"text/html")

let formatEnergyDegradationOverYears (ed: EnergyDegradationOverYears) (writer: System.IO.TextWriter) =
   writer.WriteLine("<h3>{0} with Degradation</h3>", ed.SerieName)
   writer.WriteLine("<table><thead><tr><th>Time</th>")
   ed.Years |> List.map (fun i-> i.ToString()) |> List.iter (fun i -> writeHeader writer i)
   writer.WriteLine("</tr><thead><tbody>")
   let first100Rows = ed.Rows |> List.take 100
   for (d, r) in first100Rows do
      writer.Write("<tr><td>{0:dd}/{0:MM} H={0:HH}</td>", d)
      for v in r do
         write0decCol writer v
      writer.WriteLine("</tr>")
   writer.WriteLine("</tbody></table>")

Formatter.Register<EnergyDegradationOverYears>(formatEnergyDegradationOverYears, "text/html")

// Biomassa
Formatter.SetPreferredMimeTypesFor(typeof<BiomassGasifierOutput> ,"text/html")

let formatBiomassGasifierOutput (b: BiomassGasifierOutput) (writer: System.IO.TextWriter) =
   writer.WriteLine("<h3>Biomass</h3>")
   writer.WriteLine(sprintf "Thermal Power = %.0f MWth</br>" b.ThermalPower)
   writer.WriteLine(sprintf "Max Biomass consumption 1h = %.1f t/h</br>" b.MaxBiomassConsumptionOneHour)
   writer.WriteLine(sprintf "Biomass Consumption = %.1f kgBiomass/kWh</br>" b.BiomassConsumption)
   writer.WriteLine(sprintf "Minimum Load = %.1f Mwel</br>" b.MinimumLoad)

Formatter.Register<BiomassGasifierOutput>(formatBiomassGasifierOutput, "text/html")

// ElectrolyzersOutput
Formatter.SetPreferredMimeTypesFor(typeof<ElectrolyzersOutput> ,"text/html")

let formatElectrolyzersOutput (output: ElectrolyzersOutput) (writer: System.IO.TextWriter) =
   writer.WriteLine("<h3>Electrolyzers Data Output</h3>")
   writer.WriteLine(sprintf "<p>Power DC Consumption Total = %.1f MW</br>" output.PowerDcConsumptionTot)
   writer.WriteLine(sprintf "Nominal H2 Production Total = %.0f kgH2/h</br>" output.NominalH2ProductionTot)
   writer.WriteLine(sprintf "WaterConsumption Total = %.4f tonH20/kgH2</br>" output.WaterConsumption)
   writer.WriteLine(sprintf "Water Discharged Total = %.3f tonH20/kgH2</br>" output.WaterDischarged)
   writer.WriteLine(sprintf "Oxygen Production Total = %.0f kgO2/kgH2</br>" output.OxygenProduction)
   writer.WriteLine(sprintf "Hydrogen Specific Volume NTP = %.2f Nm3/kg</br>" output.HydrogenSpecificVolumeNTP)
   writer.WriteLine(sprintf "Minimum Load Of One Line = %.0f MW</p>" output.MinimumLoadOfOneLine)
   writer.WriteLine("<h3>One Line at different loads</h3>")
   writer.WriteLine("<table><thead><tr><th>Load</th><th>Water Prod</th><th>Compress. up to 80 bar</th>")
   writer.WriteLine("<th>Other AUX</th><th>Elec. Losses</th><th>Water pumps for CCWS</th><th>Compressor oxygen up to 200 bar</th></tr></thead><tbody>")
   for oldl in output.OneLineAtDifferentLoads do
     writer.WriteLine("<tr>")
     writer.WriteLine(sprintf "<td>%.2f%%</td>" oldl.Load)
     writer.WriteLine(sprintf "<td>%.2f</td>" oldl.NominalH2Production)
     writer.WriteLine(sprintf "<td>%.2f</td>" oldl.CompressorConsumption)
     writer.WriteLine(sprintf "<td>%.2f</td>" oldl.OtherAux)
     writer.WriteLine(sprintf "<td>%.2f</td>" oldl.ElectricalLosses)
     writer.WriteLine(sprintf "<td>%.2f</td>" oldl.WaterPumps)
     writer.WriteLine(sprintf "<td>%.2f</td>" oldl.CompressorOxygen)
     writer.WriteLine("</tr>")
   writer.WriteLine("</tbody></table>")
   writer.WriteLine("<h3>Energy Consumption</h3>")
   writer.WriteLine("<table><thead><tr><th>Load</th><th>Energy consumption DC</th><th>Energy consumption AC</th>")
   writer.WriteLine("<th>Energy consumption total</th></tr></thead><tbody>")
   for ec in output.EnergyConsumptionOutput do
     writer.WriteLine("<tr>")
     writer.WriteLine(sprintf "<td>%.2f%%</td>" ec.Load)
     writer.WriteLine(sprintf "<td>%.2f</td>" ec.ConsumptionDC)
     writer.WriteLine(sprintf "<td>%.2f</td>" ec.ConsumptionAC)
     writer.WriteLine(sprintf "<td>%.2f</td>" ec.ConsumptionTot)
     writer.WriteLine("</tr>")
   writer.WriteLine("</tbody></table>")
   writer.WriteLine("<h3>Consumption with degradation at partial load</h3>")
   writer.WriteLine("<table><thead><tr><th>Year</th>")
   writer.WriteLine("""<th colspan="4">DC consumption</th>""")
   writer.WriteLine("""<th colspan="4">TOT consumption</th>""")
   writer.WriteLine("""<th colspan="4">Specific consumption</th>""")
   writer.WriteLine("<th>Slope</th><th>Intercept</th><th>Min Load</th></tr>")
   writer.WriteLine("<tr><th></th>")
   for idx in [1..3] do
      for load in output.ConsumptionOverYears.Head.Loads do
         writer.WriteLine(sprintf "<th>%.2f%%</th>" load)
   writer.WriteLine("<th></th><th></th><th></th></tr></thead><tbody>")
   for item in output.ConsumptionOverYears do
      writer.WriteLine(sprintf "<tr><td>%i</td>" item.Year)
      for dc in item.DcConsum1LineWithDegradationAtPartialLoad do
         writer.WriteLine(sprintf "<td>%.0f</td>" dc)
      for tot in item.TotConsum1LineWithDegradationAtPartialLoad do
         writer.WriteLine(sprintf "<td>%.0f</td>" tot)
      for spec in item.SpecificConsumption do
         writer.WriteLine(sprintf "<td>%.2f</td>" spec)
      writer.WriteLine(sprintf "<td>%.5f</td><td>%.4f</td><td>%.3f</td></tr>" item.Slope item.Intercect item.MinimumLoadOfOneLine)
   writer.WriteLine("</tbody></table>")

Formatter.Register<ElectrolyzersOutput>(formatElectrolyzersOutput, "text/html")

//CalculationYearRow
Formatter.SetPreferredMimeTypesFor(typeof<CalculationYearRow> ,"text/html")

let formatCalculationYearRow (cyr: CalculationYearRow) (writer: System.IO.TextWriter) =
   writer.WriteLine("Date-time = {0}</br>", cyr.Day)
   printAsInt writer "PV with degradation" cyr.PvOut
   printAsInt writer "Wind with degradation" cyr.WindOut
   printAsInt writer "NpResNonProgrammable" cyr.NpResNonProgrammable
   printAsInt writer "MaxLoad" cyr.MaxLoad
   printAsInt writer "NpResMinusMaxLoad" cyr.NpResMinusMaxLoad
   printAsInt writer "PResProgrammable" cyr.PResProgrammable
   printAsInt writer "BiomassForEEProduction" cyr.BiomassForEEProduction
   printAsInt writer "NpResAddPResMinusMaxLoad" cyr.NpResAddPResMinusMaxLoad
   printAsInt writer "ToElectrolyzer" cyr.ToElectrolyzer
   printAsInt writer "NeededFromStorage" cyr.NeededFromStorage
   printAsInt writer "PotentiallyToStorage" cyr.PotentiallyToStorage
   printAsInt writer "PotentiallyFromStorage" cyr.PotentiallyFromStorage
   printAsInt writer "BatterySoC" cyr.BatterySoC
   printAsInt writer "ChargingDischarging" cyr.ChargingDischarging
   printAsInt writer "ActualFromStorage" cyr.ActualFromStorage
   printAsInt writer "EEToElectrolyser" cyr.EEToElectrolyser
   printAsInt writer "PotentialEEFromGrid" cyr.PotentialEEFromGrid
   writer.WriteLine(sprintf "Lines Working = %i</br>" cyr.LinesWorking)
   printAsInt writer "EEToEachLine" cyr.EEToEachLine
   writer.WriteLine(sprintf "AreElectrolyzersWorkingUnderMiniumLoad = %i</br>" cyr.AreElectrolyzersWorkingUnderMiniumLoad)
   printAs2dec writer "SpecificConsumption" cyr.SpecificConsumption
   printAsInt writer "Module1" cyr.Module1
   printAsInt writer "Module2" cyr.Module2
   printAsInt writer "Module3" cyr.Module3
   printAsInt writer "TotalH2Production" cyr.TotalH2Production
   writer.WriteLine(sprintf "HourWithoutH2Production = %i</br>" cyr.HourWithoutH2Production)
   printAsInt writer "WaterConsumption" cyr.WaterConsumption
   printAs2dec writer "WaterDischarge" cyr.WaterDischarge
   printAsInt writer "EnergyToGrid" cyr.EnergyToGrid
   printAsInt writer "O2Production" cyr.O2Production
   printAsInt writer "N2Consumption" cyr.N2Consumption
   printAsInt writer "H2ToBeProduced" cyr.H2ToBeProduced
   printAsInt writer "EnergyFromGrid" cyr.EnergyFromGrid

Formatter.Register<CalculationYearRow>(formatCalculationYearRow, "text/html")

//CalculationYearOutput
Formatter.SetPreferredMimeTypesFor(typeof<CalculationYearOutput> ,"text/html")

let formatCalculationYearOutput (co: CalculationYearOutput) (writer: System.IO.TextWriter) =
   writer.WriteLine("<h3>Calculation Year {0} - {1} (first 100 rows)</h3>", co.YearOfOperation, co.Year)
   writer.WriteLine("<table><thead><tr>")
   [
     "DT"
     "PV"
     "Wind"
     "NpResNonP"
     "MaxLoad"
     "NpResMinML"
     "PResP"
     "BiomForEE"
     "NpResAddPResMinML"
     "ToElectrolyzer"
     "NeededFromStorage"
     "PotentiallyToStorage"
     "PotentiallyFromStorage"
     "BatterySoC"
     "ChargingDischarging"
     "ActualFromStorage"
     "EEToElectrolyser"
     "PotentialEEFromGrid"
     "LinesWorking"
     "EEToEachLine"
     "ElecWorkUnderMin"
     "SpecificConsum"
     "Module1"
     "Module2"
     "Module3"
     "TotalH2Production"
     "HourWithoutH2Prod"
     "WaterConsumption"
     "WaterDischarge"
     "EnergyToGrid"
     "O2Production"
     "N2Consumption"
     "H2ToBeProduced"
     "EnergyFromGrid"
   ] |> List.iter (fun item -> writeHeader writer item)
   writer.WriteLine("</tr><thead><tbody>")
   let first100Rows = co.Rows |> List.take 100
   for cyr in first100Rows do
      writer.WriteLine("<tr>")
      writer.WriteLine("<td>{0}</td>", cyr.Day)
      write0decCol writer cyr.PvOut
      write0decCol writer cyr.WindOut
      write0decCol writer cyr.NpResNonProgrammable
      write0decCol writer cyr.MaxLoad
      write0decCol writer cyr.NpResMinusMaxLoad
      write0decCol writer cyr.PResProgrammable
      write0decCol writer cyr.BiomassForEEProduction
      write0decCol writer cyr.NpResAddPResMinusMaxLoad
      write0decCol writer cyr.ToElectrolyzer
      write0decCol writer cyr.NeededFromStorage
      write0decCol writer cyr.PotentiallyToStorage
      write0decCol writer cyr.PotentiallyFromStorage
      write0decCol writer cyr.BatterySoC
      write0decCol writer cyr.ChargingDischarging
      write0decCol writer cyr.ActualFromStorage
      write0decCol writer cyr.EEToElectrolyser
      write0decCol writer cyr.PotentialEEFromGrid
      writeintCol writer cyr.LinesWorking
      write0decCol writer cyr.EEToEachLine
      writeintCol writer cyr.AreElectrolyzersWorkingUnderMiniumLoad
      write2decCol writer cyr.SpecificConsumption
      write0decCol writer cyr.Module1
      write0decCol writer cyr.Module2
      write0decCol writer cyr.Module3
      write0decCol writer cyr.TotalH2Production
      writeintCol writer cyr.HourWithoutH2Production
      write0decCol writer cyr.WaterConsumption
      write2decCol writer cyr.WaterDischarge
      write0decCol writer cyr.EnergyToGrid
      write0decCol writer cyr.O2Production
      write0decCol writer cyr.N2Consumption
      write0decCol writer cyr.H2ToBeProduced
      write0decCol writer cyr.EnergyFromGrid
      writer.WriteLine("</tr>")
   writer.WriteLine("</tbody></table>")
      
Formatter.Register<CalculationYearOutput>(formatCalculationYearOutput, "text/html")